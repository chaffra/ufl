===========================
UFL - Unified Form Language
===========================

The Unified Form Language (UFL) is a domain specific language for
declaration of finite element discretizations of variational
forms. More precisely, it defines a flexible interface for choosing
finite element spaces and defining expressions for weak forms in a
notation close to mathematical notation.

UFL is part of the FEniCS Project.

For more information, visit http://www.fenicsproject.org


Documentation
=============

The UFL documentation can be viewed at Read the Docs:

+--------+-------------------------------------------------------------------------------------+
|UFL     |  .. image:: https://readthedocs.org/projects/fenics-ufl/badge/?version=latest       |
|        |     :target: http://fenics.readthedocs.io/projects/ufl/en/latest/?badge=latest     |
|        |     :alt: Documentation Status                                                      |
+--------+-------------------------------------------------------------------------------------+


License
=======

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
